import { uniStorage } from '@/store'
import { defineStore } from 'pinia'
import GetSystemInfoResult = UniNamespace.GetSystemInfoResult

export const useAppStore = defineStore('app', {
  state: () => ({
    tokenTime: 0,
    openId: '' as string,
    accessToken: '' as string,
    userId: undefined as undefined | number,
    systemInfo: {} as GetSystemInfoResult,
    launchOptionsInfo: {} as UniApp.GetLaunchOptionsSyncOptions,
  }),
  actions: {
    updateToken(token = '', openId = '') {
      this.tokenTime = Date.now()
      this.accessToken = token
      this.openId = openId
    },
    updateUserId(s?: number) {
      this.userId = s
    },
    updateSystemInfo(s: GetSystemInfoResult) {
      this.systemInfo = s
    },
    updateLaunchOptionsInfo(s: UniApp.GetLaunchOptionsSyncOptions) {
      this.launchOptionsInfo = s
    },
  },
  persist: {
    enabled: true,
    strategies: [
      {
        storage: uniStorage,
        paths: ['accessToken', 'userId', 'answerMode', 'tokenTime', 'dailyQesIds'],
      },
    ],
  },
})
