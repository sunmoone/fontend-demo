// 完成题目的枚举
export enum ScoreActionType {
  '第一次登录' = 'firstLogin',
  '完成题目' = 'completeProblem',
  '完成刷题任务' = 'completeDailyBrushingTasks',
  '分享' = 'share'
}
