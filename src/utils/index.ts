export const isDev = import.meta.env.VITE_USER_NODE_ENV === 'development'
export const currentPlatform = uni.getSystemInfoSync().platform
export const isIOS = currentPlatform === 'ios'
export const pageCtx = () => getCurrentPages().at(-1)
