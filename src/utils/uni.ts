export const showToast = (title: string, icon: 'none' | 'success' | 'loading' | 'error' = 'none') => {
  if (!title) return Promise.resolve()
  return uni.showToast({ title, icon, duration: 2000 })
}

