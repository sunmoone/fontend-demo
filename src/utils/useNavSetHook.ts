import { useAppStore } from '@/store/app'
const appStore = useAppStore()
const { osName } = appStore.systemInfo
export default () => {
  if (osName === 'windows') {
    // 如果是电脑打开，则需要设置字体颜色
    uni.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#2cbc99',
    })
  }
}
