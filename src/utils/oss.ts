export const ossHOST = import.meta.env.VITE_OSS_HOST
export const ossDownHost = import.meta.env.VITE_OSS_DOWN_HOST
export const ossURL = (path: string) => `${ossDownHost}/${path}`
