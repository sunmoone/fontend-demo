import uniCrazyRouter from 'uni-crazy-router'
import { loginCheck } from './interceptors'

type NavToOptions = UniApp.NavigateToOptions | UniNamespace.NavigateToOptions
type NavRedirectOptions = UniApp.RedirectToOptions | UniNamespace.RedirectToOptions
type NavLaunchOptions = UniApp.ReLaunchOptions | UniNamespace.ReLaunchOptions
type NavTabOptions = UniApp.SwitchTabOptions | UniNamespace.SwitchTabOptions

type CommonOptions = NavToOptions | NavRedirectOptions | NavLaunchOptions | NavTabOptions

export type NavKey = 'to' | 'redirect' | 'launch' | 'tab'
export const navTo = (to: string | CommonOptions, type: NavKey = 'to'): Promise<void> => {
  const NavFuncMap = { to: uni.navigateTo, redirect: uni.redirectTo, launch: uni.reLaunch, tab: uni.switchTab }
  const func = NavFuncMap[type]
  console.log('run nav')
  if (typeof to === 'string') {
    // 自动添加 /pages/ 的前缀
    const url = /^\/?pages\//.test(to) ? to : `/pages/${to}`
    // @ts-ignore
    return func({ url })
  }
  // @ts-ignore
  return func(to)
}

const installFunc = uniCrazyRouter.install
uniCrazyRouter.install = (...args: never[]) => {
  installFunc(...args)
  loginCheck(uniCrazyRouter)
}
export const Router = uniCrazyRouter
