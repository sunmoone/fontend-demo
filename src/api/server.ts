import { Api } from '@/api/index'
interface IAuthResult {
  token: string
  openid: string
  userId: number
  session_key: string
}
// 认证授权auth
export const authApi = (code: string) => {
  return Api.post<IAuthResult>({
    url: '/user/authentication',
    data: {
      code,
    },
  })
}
