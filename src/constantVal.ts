const tabbarPages = [
  { name: '首页', path: '/pages/home', icon: 'home' },
  { name: '刷题', path: '/pages/answer', icon: 'photo' },
  { name: '我的', path: '/pages/user', icon: 'account' },
]

export default {
  tabbarPages,
}
