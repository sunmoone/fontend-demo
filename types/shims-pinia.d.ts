// shims-pinia.d.ts

import { Pinia, DefineStore } from 'pinia'

declare module 'pinia' {
  export function useStore(): Pinia
}

declare module '@pinia/app' {
  export const useAppStore: DefineStore<
    'app',
    {
      accessToken: string
      currentPage: string
      systemInfo: {
        [index: string]: unknown
      } // 根据实际需要定义类型
      // 其他属性和方法
    }
  >
}
