import jwt from 'jsonwebtoken'

export function verifyToken(req, res, next) {
  const token = req.headers.authorization
  const jwtSecret = process.env.JWT_SECRET // 使用环境变量，如果没有则使用默认值

  if (!token) {
    return res.status(401).json({ message: '未提供 token' })
  }
  const result = token.substring(7)
  jwt.verify(result, jwtSecret, (err, decoded) => {
    if (err) {
      return res.status(403).json({ message: '无效的 token' })
    }
    if (req.body === undefined) {
      req.body = {}
    }
    console.log(decoded, 'decoded')
    req.body.user = decoded // 将解码后的数据保存在请求对象中
    next()
  })
}
