export interface SendResponseOptions<T = any> {
  type?: 'Success' | 'Fail'
  message?: string
  data?: T
  code?: number
}

export function sendResponseSync<T>(options: SendResponseOptions<T> = { type: 'Success' }) {
  options.type = options.type ?? 'Success'
  if (options.type === 'Success') {
    return {
      message: options.message ?? null,
      data: options.data ?? null,
      status: options.type,
      code: options.code ?? 200,
    }
  }
  return {
    message: options.message ?? 'Fail',
    data: options.data ?? null,
    status: options.type,
    code: options.code ?? 400,
  }
}

// 生成uuid
export function randomUUID() {
  const s = []
  const hexDigits = '0123456789abcdef'
  for (let i = 0; i < 22; i++)
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1)

  s[14] = '4'
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1)
  const uuid = s.join('')
  return uuid
}

export function getNowDate() {
  // 创建一个当前日期对象
  const currentDate = new Date()
  // 创建一个表示北京时间的日期对象
  const beijingOptions = { timeZone: 'Asia/Shanghai' }
  const beijingTimeString = currentDate.toLocaleString('en-US', beijingOptions)
  return new Date(beijingTimeString)
}
