import log4js from 'log4js'

class Logger {
  logger: log4js.Logger
  errorLogger: log4js.Logger
  constructor() {
    log4js.configure({
      appenders: {
        file: {
          type: 'file',
          filename: 'logs/app.log',
          encoding: 'utf-8', // 指定编码为 UTF-8
        },
        console: {
          type: 'console',
          layout: {
            type: 'pattern',
            pattern: '%[[%d{yyyy-MM-dd hh:mm:ss}] [%p]%] - %m',
          },
          logColors: {
            ERROR: 'red', // 设置 ERROR 级别日志为红色
            WARN: 'yellow', // 设置 WARN 级别日志为黄色
            INFO: 'blue', // 设置 INFO 级别日志为蓝色
            DEBUG: 'green', // 设置 DEBUG 级别日志为绿色
          },
        },
      },
      categories: {
        default: {
          appenders: ['console', 'file'],
          level: 'debug',
        },
      },
    })

    this.logger = log4js.getLogger('console')
    this.errorLogger = log4js.getLogger('file')
  }

  error(msg, ...args) {
    this.errorLogger.error(msg, ...args)
  }

  info(msg, ...args) {
    this.logger.info(msg, ...args)
  }

  debug(msg, ...args) {
    this.logger.debug(msg, ...args)
  }

  warn(msg, ...args) {
    this.logger.warn(msg, ...args)
  }
}

export default new Logger()
