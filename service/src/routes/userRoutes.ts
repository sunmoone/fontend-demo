import express from 'express'
import { userAuthenticationController } from '../controller/userController'
// token中间件
// import { verifyToken } from '../middleware/verifyToken'
const userRouter = express.Router()
/**
 * 微信用户授权
 */
userRouter.post('/authentication', userAuthenticationController)

export default userRouter
