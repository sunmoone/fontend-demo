import type { FetchFn } from 'chatgpt'

import type { Request } from 'express'
export interface RequestParams<T> extends Request {
  body: T
}

export interface TokenInfo {
  openid: string
  userId: number
}

export interface RequestProps {
  prompt: string
  options?: ChatContext
  systemMessage: string
  userInfo?: any
  model?: string
}

export interface ChatContext {
  conversationId?: string
  parentMessageId?: string
}

export interface ChatGPTUnofficialProxyAPIOptions {
  accessToken: string
  apiReverseProxyUrl?: string
  model?: string
  debug?: boolean
  headers?: Record<string, string>
  fetch?: FetchFn
}

export interface ModelConfig {
  apiModel?: ApiModel
  reverseProxy?: string
  timeoutMs?: number
  socksProxy?: string
  httpsProxy?: string
  balance?: string
}

export enum AccountSource {
  '微信小程序' = 'wx',
  '微信公众号' = 'wxGzh',
}

export enum FeedBackStatus {
  '待处理' = 'pending',
  '已处理' = 'end',
}

export enum FeedBackType {
  '现有问题建议修改' = 'questionsEdit',
  '建议' = 'Suggestion',
}

export enum QuestionsStatus {
  '审核通过' = 'normal',
  '待审核' = 'pending',
  '已删除' = 'delete',
}

export enum ScoreActionType {
  '第一次登录' = 'firstLogin',
  '完成题目' = 'completeProblem',
  '完成刷题任务' = 'completeDailyBrushingTasks',
  '分享' = 'share',
}

export interface PageListResult<T> {
  total: number
  data: T
}

export type ApiModel = 'ChatGPTAPI' | 'ChatGPTUnofficialProxyAPI' | undefined
