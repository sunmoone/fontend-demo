# 前端学习助手框架
相信作为一个前端开发，都有一个上线自己的一个应用的想法吧，一个网站，一个小程序，或者一个APP。  
前段时间我就做了一个小程序（从需求到开发到上线都是一个人搞定的），我看很多小伙伴都挺感兴趣的，就想着把搭好的框架分享出来，给大家使用。  
该框架主要是提供给前端开发者想自己做一个跨端应用而提供的(服务端为nodeJs)，个人认为搭框架还是比较费时间的，而且对技术的提升没什么帮助，就想着直接把搭建好的分享给大家使用，给大家节省时间，基于搭建好的框架，大家就只需要写业务代码了，节省搭建的一些时间，减少一些阻力，希望大家不要倒在第一步。  

框架除了会内置**工程化**相关的功能外，后续会提供一些公共的业务功能支持，比如说：小程序登录认证（包括前后端代码），`OSS`文件上传，数据库连接到数据增删改查等。

## 仓库
```bash
# ssh地址：git@gitee.com:zhu_jie/fontend-demo.git
# https地址：https://gitee.com/zhu_jie/fontend-demo.git

# clone到本地
git clone git@gitee.com:zhu_jie/front-end-demo.git
```
## 框架支持
- [x] `uniApp+vite+vue3+ts`
- [x] 网络请求封装,及接口请求示例
- [x] 状态管理`pinia`及持久化配置
- [x] 类型自动导入`unplugin-auto-import`
- [x] 小程序认证示例(未使用云开发)，服务端认证
- [x] 服务端`node`环境+`express`搭建+`ts`支持
- [x] 服务端中间件`middleware`拦截示例
- [x] 数据库连接工具封装`dbUtil.ts`
- [x] 环境变量配置及使用`cross-env`
- [x] 代码校验相关工具接入`husky`, `lint-staged`, `eslint`, `prettier`
- [x] 日志工具，`log4js`配置
- [X] 服务端安全，防攻击工具`expressRateLimit`
- [x] 服务端`docker`打包部署配置
- [ ] `OSS`认证及文件上传
- [ ] `mySql`数据库连接及增删改查

## 技术栈
#### 前端
| 类别   | 库/技术栈                                |
|------|--------------------------------------|
| 整体架构 | uni-app + vite + vue3 + TS           |
| 包管理器 | pnpm                                 |
| 网络请求 | uni-ajax                             |
| 路由拦截 | uni-crazy-router                     |
| 代码校验 | husky, lint-staged, eslint, prettier |
| 状态管理 | pinia, pinia-plugin-persist-uni      |
| 类型导入 | types-sync, unplugin-auto-import     |
| 部署发布 | 小程序官方部署工具                      |

整体采用`uni-app` + `vite` + `vue3` + `ts`来实现的。不得不说`vue3`跟`ts`的结合已经相当舒服了。

#### 后端
| 类别   | 库/技术栈                                |
|------|--------------------------------------|
| 整体架构 | nodeJs + express + TS                |
| 包管理器 | pnpm                                 |
| 网络请求 | axios                                |
| 日志工具 | log4js                               |
| 代码校验 | husky, lint-staged, eslint, prettier |
| 启动器  | esno                                 |
| 打包   | tsup                                 |
| 发布   | docker                               |

## 主要目录说明
#### 前端
前端使用vue-cli基于vite脚手架构建

业务代码集中在`src`目录  
1. `src/pages`为页面目录   
2. `src/static`放一些静态资源,字体，图片等  
3. `src/store`是`pinia`配置目录，配合`uniStorage`支持持久化  
4. `src/api`存放封装好的请求工具,已实现`token`自动注入及拦截，接口请求放在`src/api/server.ts`  
5. `build`打包配置目录  
6. `DockerfileEnd`为服务端`docker`部署配置文件

#### 服务端
代码在`./service`目录，使用`express`框架，采用类似`MVC`的结构

1. `./src/index.ts`为程序启动入口文件
2. `./src/routes`为路由目录，所有的接口入口都在这里，这里会做中间件路由拦截和导航
3. `./src/controller`为路由后的业务处理，主要是调动`service`服务
4. `./src/service`主要是数据库相连，将处理好的结果交给`controller`


## 启动应用
#### 参数配置
```bash
# 小程序appid是必须要配置的
# 配置路径
# 根目录.env-> VITE_MP_APPID
# 服务端认证需要配置小程序APP_ID及APP_SECRET
# /service/.env
```

#### 安装依赖及启动
```bash
# 安装前端依赖,注意，要求node v16+版本
 pnpm i
 cd service
 # 安装服务端依赖
 pnpm i
 # 启动前端服务
 pnpm run dev
 # 启动服务端
 cd service
 pnpm run dev
 # 打开微信开发者工具，选择./dist/dev/mp-weixin 就可以了
```
