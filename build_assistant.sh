# 进入执行目录
cd /www/wwwroot/front-end-assistant
# 还原代码
git checkout .
# 拉取代码
git pull
# 停止原来的镜像
docker stop helper
# 尝试停止 Docker 容器
docker stop helper

# 检查 docker stop 的退出状态码
if [ $? -eq 0 ]; then
    # 获取当前时间并将其格式化为年月日时分秒
    current_time=$(date "+%Y%m%d%H%M%S")
    mkdir "/www/wwwroot/assistant_log/$current_time"
    docker cp helper:/app/logs "/www/wwwroot/assistant_log/$current_time"
    # 如果 docker stop 成功，则尝试删除容器
    docker rm helper
else
    # 如果 docker stop 失败，可以选择执行其他操作或者什么都不做
    echo "docker stop helper failed, not removing the container."
fi

# 打包
docker build -f ./DockerfileEnd -t helper .
# 启动新镜像
docker run --name helper -d -p 0.0.0.0:3002:3002 helper
# 查看日志
docker logs -f helper
